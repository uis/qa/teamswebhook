# Teams Webhook

**Forked and developed using https://github.com/rveachkc/pymsteams as a base - thanks to https://github.com/rveachkc for the original project!**

Python Wrapper Library to send requests to Microsoft Teams Webhooks.
Microsoft refers to these messages as Connector Cards. A message can be sent with only the main Connector Card, or additional sections can be included into the message.

This library uses Webhook Connectors for Microsoft Teams. Please visit the following Microsoft Documentation link for instructions on how to obtain the correct url for your Channel: https://dev.outlook.com/Connectors/GetStarted#creating-messages-through-office-365-connectors-in-microsoft-teams

Please refer to the Microsoft Documentation for the most up-to-date screenshots.
https://dev.outlook.com/connectors/reference

## Installation

Install with pip:

```bash
pip install teamswebhook --index-url https://gitlab.developers.cam.ac.uk/api/v4/projects/6885/packages/pypi/simple
```

## Usage

### Creating ConnectorCard Messages

This is the simplest implementation of MS Teams Webhook. It will send a message to the teams webhook url with plain text in the message.

```python
import teamswebhook

# You must create the connectorcard object with the Microsoft Webhook URL
my_teams_message = teamswebhook.ConnectorCard("<Microsoft Webhook URL>")

# Add text to the message.
my_teams_message.text("this is my text")

# send the message.
my_teams_message.send()
```

Please visit the python asyncio documentation for more info on using asyncio and the event loop: https://docs.python.org/3/library/asyncio-eventloop.html

### Optional Formatting Methods for Cards

#### Add a title

```python
my_teams_message.title("This is my message title")
```

#### Add a link button

```python
my_teams_message.add_link_button("This is the button Text", "https://github.com/rveachkc/pymsteams/")
```

#### Change URL

This is useful in the event you need to post the same message to multiple rooms.

```python
my_teams_message.new_hook_url("<My New URL>")
```

#### Set Color Theme

This sets the theme color of the card. The parameter is expected to be a hex color code without the hash or the string red.

```python
my_teams_message.color("<Hex Color Code>")
```

#### Preview your object

This is a simple print command to view your connector card message object before sending.

```python
my_teams_message.print_payload()
```

### Adding sections to the Connector Card Message

To create a section and add various formatting elements

```python
# create the section
my_message_section = teamswebhook.CardSection()

# Section Title
my_message_section.title("Section title")

# Activity Elements
my_message_section.activity_title("my activity title")
my_message_section.activity_subtitle("my activity subtitle")
my_message_section.activity_image("http://i.imgur.com/c4jt321l.png")
my_message_section.activity_text("This is my activity Text")

# Facts are key value pairs displayed in a list.
my_message_section.add_fact("this", "is fine")
my_message_section.add_fact("this is", "also fine")

# Section Text
my_message_section.text("This is my section text")

# Section Images
my_message_section.add_image("http://i.imgur.com/c4jt321l.png", ititle="This Is Fine")

# Add your section to the connector card object before sending
my_teams_message.addSection(my_message_section)
```

You may also add multiple sections to a connector card message as well.

```python
# Create Section 1
import teamswebhook

section_1 = teamswebhook.CardSection()
section_1.text("My First Section")

# Create Section 2
section_2 = teamswebhook.CardSection()
section_2.text("My Second Section")

# Add both Sections to the main card object
my_teams_message.add_section(section_1)
my_teams_message.add_section(section_2)

# Then send the card
my_teams_message.send()
```

### Adding potential actions to the Connector Card Message

To create actions on which the user can interact with in MS Teams
To find out more information on what actions can be used, please visit https://docs.microsoft.com/en-us/microsoftteams/platform/concepts/connectors/connectors-using#setting-up-a-custom-incoming-webhook

```python
import teamswebhook

my_teams_message = teamswebhook.ConnectorCard("<Microsoft Webhook URL>")

my_teams_potential_action_1 = teamswebhook.PotentialAction(
  _name="Add a comment")
my_teams_potential_action_1.add_input("TextInput", "comment",
                                      "Add a comment here", False)
my_teams_potential_action_1.add_action("HttpPost", "Add Comment", "https://...")

my_teams_potential_action_2 = teamswebhook.PotentialAction(_name="Set due date")
my_teams_potential_action_2.add_input("DateInput", "dueDate", "Enter due date")
my_teams_potential_action_2.add_action("HttpPost", "save", "https://...")

my_teams_potential_action_3 = teamswebhook.PotentialAction(
  _name="Change Status")
my_teams_potential_action_3.choices.add_choices("In progress", "0")
my_teams_potential_action_3.choices.add_choices("Active", "1")
my_teams_potential_action_3.add_input("MultichoiceInput", "list",
                                      "Select a status", False)
my_teams_potential_action_3.add_action("HttpPost", "Save", "https://...")

my_teams_message.addPotentialAction(my_teams_potential_action_1)
my_teams_message.addPotentialAction(my_teams_potential_action_2)
my_teams_message.addPotentialAction(my_teams_potential_action_3)

my_teams_message.summary("Test Message")

my_teams_message.send()
```

### Adding HTTP Post to potential actions in the Connector Card Message

```python
import teamswebhook

my_teams_message = teamswebhook.ConnectorCard("<Microsoft Webhook URL>")

my_teams_potential_action_1 = teamswebhook.PotentialAction(
  _name="Add a comment")
# You can add a TextInput to your potential action  - Please note the 2nd argument below as the id name
my_teams_potential_action_1.addInput("TextInput", "comment",
                                     "Add a comment here", False)
# We use the 2nd argument above as the id name to parse the values into the body post like below.
my_teams_potential_action_1.addAction("HttpPost", "Add Comment", "https://...",
                                      "{{comment.value}}")
my_teams_message.add_potential_action(my_teams_potential_action_1)

my_teams_message.summary("Test Message")

my_teams_message.send()

# Notes:
# If you post anything via teams, you will get some Javascript encoding happening via the post - For example:
# Posting this:  {"name":"john", "comment" : "nice"}
# Output will be:  b'{\\u0022name\\u0022:\\u0022john\\u0022, \\u0022comment\\u0022 : \\u0022nice\\u0022}'
# i solved this issue by decoding unicode escape for a custom rest backend.
```

Please use Github issues to report any bugs or request enhancements.

## Troubleshooting HTTP response

This module is really just a nice wrapper pointed at the Microsoft API. To help troubleshoot missing messages, the requests response content is saved to the connectorcard class attribute `last_http_response`.

To get the last http status code:

```python
import teamswebhook

my_teams_message = teamswebhook.ConnectorCard("<Microsoft Webhook URL>")
my_teams_message.text("this is my text")
my_teams_message.send()

last_status_code = my_teams_message.last_http_response.status_code
```

More info on the Response Content is available in the requests documentation, [link](https://2.python-requests.org/en/master/user/quickstart/#response-content).

## Exceptions

If the call to the Microsoft Teams webhook service fails, a `TeamsWebhookException` will be thrown.

## Certificate Validation

In some situations, a custom CA bundle must be used. This can be set on class initialization, by setting the verify parameter.

```python
import teamswebhook

# set custom ca bundle
msg = teamswebhook.ConnectorCard("<Microsoft Webhook URL>",
                                 verify="/path/to/file")

# disable CA validation
msg = teamswebhook.ConnectorCard("<Microsoft Webhook URL>", verify=False)
```

Set to either the path of a custom CA bundle or False to disable.

The requests documentation can be referenced for full details: https://2.python-requests.org/en/master/user/advanced/#ssl-cert-verification
