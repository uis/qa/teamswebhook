import os

import pytest

import teamswebhook

# def test_env_webhook_url():
#     """
#         Test that we have the webhook set as an environment variable.
#         This is testing our test environment, not the code.
#     """
#     webhook_url = os.getenv("MS_TEAMS_WEBHOOK", None)
#     assert webhook_url
#     assert webhook_url.find("https") == 0


def test_send_message():
    """
    This sends a simple text message with a title and link button.
    """

    teams_message = teamswebhook.ConnectorCard(os.getenv("MS_TEAMS_WEBHOOK"))
    teams_message.text("This is a simple text message.")
    teams_message.title("Simple Message Title")
    teams_message.add_link_button("Go to the Repo", "https://github.com/rveachkc/pymsteams")
    # teams_message.send()
    #
    # assert isinstance(teams_message.last_http_response.status_code, int)


def test_send_sectioned_message():
    """
    This sends a message with sections.
    """

    # start the message
    teams_message = teamswebhook.ConnectorCard(os.getenv("MS_TEAMS_WEBHOOK"))
    teams_message.text("This is the main title.")
    teams_message.title("Sectioned Message Title")

    # section 1
    section_1 = teamswebhook.CardSection()
    section_1.title("Section 1 title")
    section_1.activity_title("my activity title")
    section_1.activity_subtitle("my activity subtitle")
    section_1.activity_text(
        """This is my activity Text.
        You should see an activity image, activity title, activity subtitle,
        and this text (of course)."""
    )
    section_1.add_fact("Fact", "this is fine")
    section_1.add_fact("Fact", "this is also fine")
    section_1.text(
        "This is my section 1 text.  This section has an activity above and two facts below."
    )
    teams_message.add_section(section_1)

    # section 2
    section_2 = teamswebhook.CardSection()
    section_2.text(
        "This is section 2. You should see an image. This section does not have facts or a title."
    )
    teams_message.add_section(section_2)

    # teams_message.send()
    # assert isinstance(teams_message.last_http_response.status_code, int)


def test_send_potential_action():
    """
    This sends a message with a potential action
    """

    my_teams_message = teamswebhook.ConnectorCard(os.getenv("MS_TEAMS_WEBHOOK"))
    my_teams_message.text("This message should have four potential actions.")
    my_teams_message.title("Action Message Title")

    my_teams_potential_action1 = teamswebhook.PotentialAction(_name="Add a comment")
    my_teams_potential_action1.add_input("TextInput", "comment", "Add a comment", False)
    my_teams_potential_action1.add_action(
        "HttpPost", "Add Comment", "https://jsonplaceholder.typicode.com/posts"
    )

    my_teams_potential_action2 = teamswebhook.PotentialAction(_name="Get Users")
    my_teams_potential_action2.add_input("DateInput", "dueDate", "Enter due date")
    my_teams_potential_action2.add_action(
        "HttpPost", "save", "https://jsonplaceholder.typicode.com/posts"
    )

    my_teams_potential_action3 = teamswebhook.PotentialAction(_name="Change Status")
    my_teams_potential_action3.choices.add_choices("In progress", "0")
    my_teams_potential_action3.choices.add_choices("Active", "1")
    my_teams_potential_action3.add_input("MultichoiceInput", "list", "Select a status", False)
    my_teams_potential_action3.add_action(
        "HttpPost", "Save", "https://jsonplaceholder.typicode.com/posts"
    )

    my_teams_potential_action4 = teamswebhook.PotentialAction(_name="Download teamswebhook")
    my_teams_potential_action4.add_open_uri(
        "Links",
        [
            {
                "os": "default",
                "uri": "https://pypi.org/project/pymsteams/",
            },
        ],
    )

    my_teams_message.add_potential_action(my_teams_potential_action1)
    my_teams_message.add_potential_action(my_teams_potential_action2)
    my_teams_message.add_potential_action(my_teams_potential_action3)
    my_teams_message.summary("Message Summary")

    # my_teams_message.send()
    # assert isinstance(my_teams_message.last_http_response.status_code, int)


def test_http_500():
    with pytest.raises(teamswebhook.TeamsWebhookException):
        my_teams_message = teamswebhook.ConnectorCard("https://httpstat.us/500")
        my_teams_message.text("This is a simple text message.")
        my_teams_message.title("Simple Message Title")
        my_teams_message.send()
        # my_teams_message.hook_url = "https://httpstat.us/500"


# def test_http_403():
#     with pytest.raises(teamswebhook.TeamsWebhookException):
#         my_teams_message = teamswebhook.ConnectorCard("http://httpstat.us/403")
#         my_teams_message.text("This is a simple text message.")
#         my_teams_message.title("Simple Message Title")
#         # my_teams_message.send()


# def test_message_size():
#     def get_message(_card):
#         _message = teamswebhook.ConnectorCard(os.getenv("MS_TEAMS_WEBHOOK"))
#         _message.title('Simple Message Title')
#         _message.summary('Simple Summary')
#         _message.add_section(card)
#         return _message
#
#     # setup text that's too large
#     failure_char_count = 21000
#     text = 'a' * failure_char_count
#
#     card = teamswebhook.CardSection()
#     card.text(text)
#     message = get_message(card)
#     with pytest.raises(teamswebhook.TeamsWebhookException):
#         message.send()
#
#     card1 = teamswebhook.CardSection()
#     card2 = teamswebhook.CardSection()
#     card1.text(text[:int(len(text) / 2)])
#     card2.text(text[int(len(text) / 2):])
#     message = get_message(card1)
#     # assert message.send()
#     message = get_message(card2)
#     # assert message.send()


def test_chaining():
    card = teamswebhook.CardSection()
    card.title("Foo").activity_title("Bar").activity_subtitle("Baz")
    assert card.payload["title"] == "Foo"
    assert card.payload["activityTitle"] == "Bar"
    assert card.payload["activitySubtitle"] == "Baz"

    connector_card = teamswebhook.ConnectorCard("https://example.org")
    connector_card.text("Big text").title("Cool title").summary("Something happened")
    assert connector_card.payload["title"] == "Cool title"
    assert connector_card.payload["text"] == "Big text"
    assert connector_card.payload["summary"] == "Something happened"
