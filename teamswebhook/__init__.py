import requests


class TeamsWebhookException(Exception):
    """custom exception for failed webhook call"""

    pass


class CardSection:
    def __init__(self):
        self.payload = {}

    def title(self, title):
        # title of the section
        self.payload["title"] = title
        return self

    def activity_title(self, activity_title):
        # Title of the event or action. Often this will be the name of the "actor".
        self.payload["activityTitle"] = activity_title
        return self

    def activity_subtitle(self, activity_subtitle):
        # A subtitle describing the event or action. Often this will be a summary of the action.
        self.payload["activitySubtitle"] = activity_subtitle
        return self

    def activity_image(self, activity_image):
        # URL to image or a data URI with the base64-encoded image inline.
        # An image representing the action. Often this is an avatar of the "actor" of the activity.
        self.payload["activityImage"] = activity_image
        return self

    def activity_text(self, activity_text):
        # A full description of the action.
        self.payload["activityText"] = activity_text
        return self

    def add_fact(self, fact_name, fact_value):
        if "facts" not in self.payload:
            self.payload["facts"] = []

        new_fact = {"name": fact_name, "value": fact_value}
        self.payload["facts"].append(new_fact)
        return self

    def add_image(self, image, image_title=None):
        if "images" not in self.payload:
            self.payload["images"] = []
        image_object = {"image": image}
        if image_title:
            image_object["title"] = image_title
        self.payload["images"].append(image_object)
        return self

    def text(self, text):
        self.payload["text"] = text
        return self

    def link_button(self, button_text, button_url):
        self.payload["potentialAction"] = [
            {
                "@context": "http://schema.org",
                "@type": "ViewAction",
                "name": button_text,
                "target": [button_url],
            }
        ]
        return self

    def disable_markdown(self):
        self.payload["markdown"] = False
        return self

    def enable_markdown(self):
        self.payload["markdown"] = True
        return self

    def dump_section(self):
        return self.payload


class PotentialAction:
    def __init__(self, _name, _type="ActionCard"):
        self.payload = {"@type": _type, "name": _name}
        self.choices = Choice()

    def add_input(self, _type, _id, title, is_multiline=None):
        if "inputs" not in self.payload:
            self.payload["inputs"] = []
        if self.choices.dump_choices():
            _input = {
                "@type": _type,
                "id": _id,
                "isMultiline": str(is_multiline).lower(),
                "choices": self.choices.dump_choices(),
                "title": title,
            }
        else:
            _input = {
                "@type": _type,
                "id": _id,
                "isMultiline": is_multiline,
                "title": title,
            }

        self.payload["inputs"].append(_input)
        return self

    def add_action(self, _type, _name, _target, _body=None):
        if "actions" not in self.payload:
            self.payload["actions"] = []
        action = {"@type": _type, "name": _name, "target": _target}
        if _body:
            action["body"] = _body

        self.payload["actions"].append(action)
        return self

    def add_open_uri(self, _name, _targets):
        """
        Creates a OpenURI action

        https://docs.microsoft.com/en-us/outlook/actionable-messages/message-card-reference#openuri-action

        :param _name: *Name of the text to appear inside the ActionCard*
        :type _name: str
        :param _targets: *A list of dictionaries, ex: `{"os": "default", "uri": "https://www..."}`*
        :type _targets: list(dict())
        """
        self.payload["@type"] = "OpenUri"
        self.payload["name"] = _name
        if not isinstance(_targets, list):
            raise TypeError("Target must be of type list(dict())")
        self.payload["targets"] = _targets
        return self

    def dump_potential_action(self):
        return self.payload


class Choice:
    def __init__(self):
        self.choices = []

    def add_choices(self, _display, _value):
        self.choices.append({"display": _display, "value": _value})

    def dump_choices(self):
        return self.choices


class ConnectorCard:
    def __init__(self, hook_url, http_proxy=None, https_proxy=None, http_timeout=60, verify=None):
        self.payload = {}
        self.hook_url = hook_url
        self.proxies = {}
        self.http_timeout = http_timeout
        self.verify = verify
        self.last_http_response = None

        if http_proxy:
            self.proxies["http"] = http_proxy

        if https_proxy:
            self.proxies["https"] = https_proxy

        if not self.proxies:
            self.proxies = None

    def text(self, text):
        self.payload["text"] = text
        return self

    def title(self, title):
        self.payload["title"] = title
        return self

    def summary(self, summary):
        self.payload["summary"] = summary
        return self

    def color(self, color):
        color = "E81123" if color.lower() == "red" else color
        self.payload["themeColor"] = color
        return self

    def add_link_button(self, button_text, button_url):
        if "potentialAction" not in self.payload:
            self.payload["potentialAction"] = []

        _button = {
            "@context": "http://schema.org",
            "@type": "ViewAction",
            "name": button_text,
            "target": [button_url],
        }

        self.payload["potentialAction"].append(_button)
        return self

    def new_hook_url(self, new_hook_url):
        self.hook_url = new_hook_url
        return self

    def add_section(self, new_section: CardSection):
        # this function expects a cardsection object
        if "sections" not in self.payload:
            self.payload["sections"] = []

        self.payload["sections"].append(new_section.dump_section())
        return self

    def add_potential_action(self, new_action: PotentialAction):
        # this function expects a potential action object
        if "potentialAction" not in self.payload:
            self.payload["potentialAction"] = []

        self.payload["potentialAction"].append(new_action.dump_potential_action())
        return self

    def print_payload(self):
        print(f"hookurl: {self.hook_url}")
        print(f"payload: {self.payload}")

    def send(self):
        headers = {"Content-Type": "application/json"}
        r = requests.post(
            self.hook_url,
            json=self.payload,
            headers=headers,
            proxies=self.proxies,
            timeout=self.http_timeout,
            verify=self.verify,
        )
        self.last_http_response = r

        if r.status_code == requests.codes.ok:  # pylint: disable=no-member
            return True
        else:
            raise TeamsWebhookException(r.text)


def format_url(display, url):
    return f"[{display}]({url})"
